# **PC_Building_App** 
___
#### This will be a custom config PC building App, where you can choose the various parts of the PC.

## Setting up Repo
___
* Clone this repository and install its dependencies.
> ### **npm install**

## Run the application
___
* Run the following command.
> ### **npm start**
* App will be launched in https://localhost:5000 
* Server will be listening to port 3000
---
